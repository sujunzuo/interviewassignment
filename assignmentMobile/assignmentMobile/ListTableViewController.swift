//
//  ListTableViewController.swift
//  assignmentMobile
//
//  Created by Fang Zhai on 2020-03-08.
//  Copyright © 2020 junzuo. All rights reserved.
//

import UIKit
import CoreData

class ListTableViewController: UITableViewController {
    var myRoot : YoutubeList?
    //var items: [NSManagedObject] = []
    
    let webServiceJSONDecoder = WebServiceJSONDecoder()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableView.automaticDimension
        //get data from web
        getDataFromUrl()
        
    }
    func getDataFromUrl(){
       
        let query = ["channelId": "UC_x5XG1OV2P6uZZ5FSM9Ttw",
                     "maxResults": "25",
                     "key": "AIzaSyDzeYizzRNBysludLqEBU6osYlcH3hYZSU"]
        webServiceJSONDecoder.fetchItem(matching: query, completion: {
            (result) in
            DispatchQueue.main.async {
                if let result = result{
                    self.myRoot = result
                    self.tableView.reloadData()
                    //print("got data:\(self.myRoot?.kind)")
                }else{
                    print("Unable to load data.")
                }
            }
        })
        
    }
    // MARK: save to CoreData : 
    @IBAction func saveToCoreData(_ sender: UIBarButtonItem) {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "PlaylistItem",
                                       in: managedContext)!
        // iterate myRoot?.items
        if let endpointItems = myRoot?.items {
            var countItem = 0
            for endpointItem in endpointItems{
                let item = NSManagedObject(entity: entity,insertInto: managedContext)
                item.setValue(endpointItem.snippet.title, forKeyPath: "title")
                let url = "\(endpointItem.snippet.thumbnails.medium.url)"
                item.setValue(url, forKeyPath:"thumbnailurl")
                item.setValue(endpointItem.snippet.description, forKeyPath: "listdescirption")
                item.setValue(endpointItem.contentDetails.itemCount, forKeyPath: "countofvideos")
                
                do {
                   try managedContext.save()
                       countItem += 1
                       //print("save item in: \(countItem)")
                       //items.append(item)
                } catch let error as NSError {
                  print("Could not save. \(error), \(error.userInfo)")
               }
           }
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
         return myRoot?.items.count ?? 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath)  as! TableViewCell

        // Configure the cell...
        let storeItem = myRoot!.items[indexPath.row]
        cell.update(with: storeItem)
        return cell
    }
    

    
    
   // for show DetailTableViewController
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       if segue.identifier == "toDetail" {
           let indexPath = tableView.indexPathForSelectedRow!
           let currentItem = myRoot?.items[indexPath.row]
           
           let navController = segue.destination as! UINavigationController
           let detailTableView = navController.topViewController as! DetailTableViewController
           detailTableView.currentItem = currentItem
           
       }
   }
   @IBAction func unwindToListTableView(_ unwindSegue: UIStoryboardSegue){
       
       
   }

}
