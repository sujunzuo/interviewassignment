//
//  ViewController.swift
//  assignmentMobile
//
//  Created by Fang Zhai on 2020-03-07.
//  Copyright © 2020 junzuo. All rights reserved.
//

import UIKit
import GoogleSignIn


// Match the ObjC symbol name inside Main.storyboard.
@objc(ViewController)
class ViewController: UIViewController,GIDSignInDelegate {
    
    @IBOutlet weak var signInButton: GIDSignInButton!
    @IBOutlet weak var signOutButton: UIButton!
    
    @IBOutlet weak var disconnectButton: UIButton!
    @IBOutlet weak var statusText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self

        // Automatically sign in the user.
        GIDSignIn.sharedInstance()?.restorePreviousSignIn()

        // [START_EXCLUDE]
        NotificationCenter.default.addObserver(self,
            selector: #selector(ViewController.receiveToggleAuthUINotification(_:)),
            name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
            object: nil)

        statusText.text = "Initialized Swift app..."
        toggleAuthUI()
        // [END_EXCLUDE]
    }
   // [START signout_tapped]
    @IBAction func didTapSignOut(_ sender: Any) {
        GIDSignIn.sharedInstance().signOut()
        // [START_EXCLUDE silent]
        statusText.text = "Signed out."
        toggleAuthUI()
        // [END_EXCLUDE]
    }
    // [END signout_tapped]
    
    @IBAction func didTapDisconnect(_ sender: Any) {
        GIDSignIn.sharedInstance().disconnect()
        // [START_EXCLUDE silent]
        statusText.text = "Disconnecting."
        // [END_EXCLUDE]
    }
    // [START toggle_auth]
    @IBAction func didTapSignIn(_ sender: GIDSignInButton) {
        //GIDSignIn.sharedInstance()?.signIn()
        print("press sign in")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        print(appDelegate.testStr)
        performSegue(withIdentifier: "toList", sender: nil)
    }
    
     func toggleAuthUI() {
       if let _ = GIDSignIn.sharedInstance()?.currentUser?.authentication {
         // Signed in
         signInButton.isHidden = true
         signOutButton.isHidden = false
         disconnectButton.isHidden = false
       } else {
         signInButton.isHidden = false
         signOutButton.isHidden = true
         disconnectButton.isHidden = true
         statusText.text = "Google Sign in\niOS Demo"
       }
     }
     // [END toggle_auth]
     override var preferredStatusBarStyle: UIStatusBarStyle {
       return UIStatusBarStyle.lightContent
     }

     deinit {
       NotificationCenter.default.removeObserver(self,
           name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
           object: nil)
     }

     @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
       if notification.name.rawValue == "ToggleAuthUINotification" {
         self.toggleAuthUI()
         if notification.userInfo != nil {
           guard let userInfo = notification.userInfo as? [String:String] else { return }
           self.statusText.text = userInfo["statusText"]!
         }
       }
     }
  func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
            withError error: Error!) {
    if let error = error {
      if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
        print("The user has not signed in before or they have since signed out.")
      } else {
        print("\(error.localizedDescription)")
      }
      return
    }
    
    // Perform any operations on signed in user here.
    let userId = user.userID                  // For client-side use only!
    let idToken = user.authentication.idToken // Safe to send to the server
    let fullName = user.profile.name
    let givenName = user.profile.givenName
    let familyName = user.profile.familyName
    let email = user.profile.email
    // ...
      print("***forDebug:\(String(describing: fullName))")
    
     
  }
  func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
            withError error: Error!) {
    // Perform any operations when the user disconnects from app here.
    // ...
  }

}

