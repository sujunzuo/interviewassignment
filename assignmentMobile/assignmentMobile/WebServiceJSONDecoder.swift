//
//  WebServiceJSONDecoder.swift
//  assignmentMobile
//


import Foundation
struct WebServiceJSONDecoder {
    func fetchItem(matching query: [String: String], completion: @escaping (YoutubeList?) -> Void) {
        /*let baseURL = URL(string: "https://www.googleapis.com/youtube/v3/playlists?part=snippet%2CcontentDetails&")!
        guard let url = baseURL.withQueries(query) else {
            completion(nil)
            print("Unable to build URL with supplied queries. ")
            return
        } */
        let url = URL(string:"https://www.googleapis.com/youtube/v3/playlists?part=snippet%2CcontentDetails&channelId=UC_x5XG1OV2P6uZZ5FSM9Ttw&maxResults=25&key=AIzaSyDzeYizzRNBysludLqEBU6osYlcH3hYZSU&playlistId=PLBCF2DAC6FFB574DE")!
        
        
        let task = URLSession.shared.dataTask(with: url){ (data, response, error) in
            let decoder = JSONDecoder()
            if let data = data,
                let results = try? decoder.decode(YoutubeList.self, from: data){
                completion(results)
            }else{
                print ("Either no data was returned, or data was not serialized.")
                completion(nil)
                return
                
            }
        }
        task.resume()
    }
}
