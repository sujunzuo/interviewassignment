//
//  TableViewCell.swift
//  assignmentMobile
//
//  Created by Fang Zhai on 2020-03-08.
//  Copyright © 2020 junzuo. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var myImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func update(with listItem: Item) {
        titleLabel.text = listItem.snippet.title
        countLabel.text = "It has " + String(listItem.contentDetails.itemCount) + " items"
        
        let task = URLSession.shared.dataTask(with:listItem.snippet.thumbnails.medium.url){
                (data,response,error) in
        
                guard let imageData = data else {
                    return
                }
                DispatchQueue.main.async {
                    let image = UIImage(data: imageData)
                    self.myImage.image = image
                }
            }
            task.resume()
    }
}
