//
//  YoutubeList.swift
//  assignmentMobile
//
//  Created by Fang Zhai on 2020-03-08.
//  Copyright © 2020 junzuo. All rights reserved.
//

import Foundation
struct YoutubeList: Codable {
    var kind: String
    var etag : String
    var nextPageToken: String
    var items: [Item]
}
struct Item : Codable {
    let kind : String
    let etag : String
    let id : String
    let snippet : Snippet
    let contentDetails : ContentDetail
   
    
}
struct Snippet : Codable {
    let title : String
    let description : String
    let thumbnails : Thumbnail
    
}
struct ContentDetail : Codable {
    let itemCount : Int
}
struct Thumbnail : Codable {
    let medium : Medium
}
struct Medium : Codable {
    let url : URL
    let width : Int
    let height : Int
}
